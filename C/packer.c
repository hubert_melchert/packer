#include <stdio.h>
#include <stdint.h>

static uint8_t STX = 0x02;
static uint8_t DLE = 0x10;
static uint8_t ETX = 0x03;

#define NELEMS(array) (sizeof(array) / sizeof(array[0]))

void print_array(uint8_t *array, int nelems)
{
    for(int i = 0; i < nelems; i++)
    {
        printf("%d ", array[i]);
    }
    printf("\n");
}
int32_t pack(uint8_t *restrict dataIn, uint8_t *restrict dataOut, uint16_t sizeIn, uint16_t sizeOut)
{
    if(dataIn == NULL || dataOut == NULL || sizeIn == 0 || sizeOut == 0)
    {
        return -1;
    }

    dataOut[0] = STX;
    uint32_t packedSize = 1;

    for(uint32_t i = 0; i < sizeIn; i++)
    {
        if(packedSize >= sizeOut)
        {
            return -1;
        }

        if(dataIn[i] == STX || dataIn[i] == DLE || dataIn[i] == ETX)
        {
            dataOut[packedSize++] = DLE;
            if(packedSize >= sizeOut)
            {
                return -1;
            }
            dataOut[packedSize++] = dataIn[i] + 0x80;
        }
        else
        {
            dataOut[packedSize++] = dataIn[i];
        }
    }
    if(packedSize + 1 < sizeOut)
    {
       dataOut[packedSize++] = ETX; 
    }
    return packedSize;
}

int32_t unpack(uint8_t *dataIn, uint8_t *dataOut, uint32_t sizeIn, uint32_t sizeOut)
{
    if(dataIn == NULL || dataOut == NULL || 
       sizeIn < (sizeof(STX) + sizeof(ETX) + sizeof(uint8_t))
       || dataIn[0] != STX || dataIn[sizeIn - 1] != ETX)
    {
        return -1;
    }

    uint32_t unpackedSize = 0;
    sizeIn -= sizeof(ETX);

    for(uint32_t i = 1; i < sizeIn; i++)
    {
        if(unpackedSize >= sizeOut)
        {
            return -2;
        }
        if(dataIn[i] == DLE)
        {
            if(i + 1 >= sizeIn)
            {
                return -1;
            }
            dataOut[unpackedSize++] = dataIn[++i] - 0x80;
        }
        else
        {
            dataOut[unpackedSize++] = dataIn[i];
        }
    }

    return unpackedSize;
}

int main(int argc, char *argv[])
{
    uint8_t data[5] = {4, 5, 6, 7, 0x10};
    uint8_t unpackedData[5] = {0};
    uint8_t packedData[10] = {0};

    int32_t packedSize = pack(data, packedData, sizeof(data), sizeof(packedData));
    printf("Pack = %d\r\n", packedSize);

    print_array(data, sizeof(data));
    print_array(packedData, sizeof(packedData));

    int32_t unpackedSize = unpack(packedData, unpackedData, packedSize, sizeof(unpackedData));
    printf("Unpack = %d\r\n", unpackedSize);
    print_array(unpackedData, sizeof(unpackedData));
    return 0;
}

