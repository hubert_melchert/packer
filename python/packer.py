STX = 0x02
DLE = 0x10
ETX = 0x03


def pack(data):
    packed_data = [STX]

    for d in data:
        if not (0 <= d <= 255):
            raise ValueError
        if d in [STX, DLE, ETX]:
            d += 0x80
            if d > 255:
                d -= 255
            packed_data += [DLE, d]
        else:
            packed_data.append(d)

    packed_data.append(ETX)
    return packed_data

def unpack(data):
    unpacked_data = []

    if data[0] != STX or data[-1] != ETX:
        raise ValueError
    iterable_data = iter(data[1:len(data) - 1])

    for d in iterable_data:
        if d == DLE:
            d = next(iterable_data, None) - 0x80
            if d < 0:
                d += 255
        if not (0 <= d <= 255):
            raise ValueError
        unpacked_data.append(d)

    return unpacked_data 


data = [4, 5, 6, 7, 0x10]
packed = pack(data)
unpacked = unpack(packed)
print data
print packed
print unpacked


