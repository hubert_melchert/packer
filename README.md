#PACKER
**Control bytes**

* STX = 0x02
* DLE = 0x10
* ETX = 0x03

###Overview
Module provides pack and unpack ability based on NET0 protocol. Provided data is not required to have any specific form, module is transparent when it comes to data structure. 
###Packing
Every packed frame is guaranteed to start with STX and end with ETX. In packed frame every byte of original data that was equal to any of control bytes is preceeded with DLE byte add incremented by 0x80. 
###Unpacking
When unpacking every byte equal to DLE is skipped and next byte decremented by 0x80 is taken as data.	

